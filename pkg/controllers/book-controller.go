package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/qualibre-info-formations/public/birdz-backend-demo/pkg/models"
	"gitlab.com/qualibre-info-formations/public/birdz-backend-demo/pkg/utils"
	"net/http"
	"strconv"
)

func CreateBook(w http.ResponseWriter, r *http.Request) {
	CreateBook := &models.Book{}
	utils.ParseBody(r, CreateBook)
	b := CreateBook.CreateBook()
	res, err := json.Marshal(b)
	if err != nil {
		w.WriteHeader(http.StatusExpectationFailed)
	} else {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(res)
	}
}

func GetBooks(w http.ResponseWriter, _ *http.Request) {
	newBooks := models.GetAllBooks()
	res, err := json.Marshal(newBooks)
	if err != nil {
		w.WriteHeader(http.StatusExpectationFailed)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(res)
	}
}

func GetBookById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bookId := vars["bookId"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing book ID", bookId)
		w.WriteHeader(http.StatusExpectationFailed)
	} else {
		bookDetails, _ := models.GetBookById(ID)
		res, err := json.Marshal(bookDetails)
		if err != nil {
			w.WriteHeader(http.StatusExpectationFailed)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write(res)
		}
	}
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
	var updateBook = &models.Book{}
	utils.ParseBody(r, updateBook)
	vars := mux.Vars(r)
	bookId := vars["bookId"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing book ID", bookId)
		w.WriteHeader(http.StatusExpectationFailed)
	} else {
		bookDetails, db := models.GetBookById(ID)
		if updateBook.Name != "" {
			bookDetails.Name = updateBook.Name
		}
		if updateBook.Author != "" {
			bookDetails.Author = updateBook.Author
		}
		if updateBook.Publication != "" {
			bookDetails.Publication = updateBook.Publication
		}
		db.Save(&bookDetails)
		res, err := json.Marshal(bookDetails)
		if err != nil {
			w.WriteHeader(http.StatusExpectationFailed)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write(res)
		}
	}
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bookId := vars["bookId"]
	ID, err := strconv.ParseInt(bookId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing book ID", bookId)
		w.WriteHeader(http.StatusExpectationFailed)
	} else {
		book := models.DeleteBook(ID)
		res, err := json.Marshal(book)
		if err != nil {
			w.WriteHeader(http.StatusExpectationFailed)
		} else {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write(res)
		}
	}
}
